const dataloader = require('../')

function whereIn (val, keys) {
  return new Promise(function (resolve) {
    resolve(keys.map(key => ({ id: key })))
  })
}

function db (table) {
  return query
}

const query = {
  whereIn,
}

const dl = dataloader('models', { db })
const dlRuntimeOpts = dataloader('models')

test('should return fn', () => {
  expect(typeof dataloader).toBe('function')
})

test('should return fn after options set', () => {
  expect(typeof dl).toBe('function')
})

test('should create dataloader', () => {
  const loaders = dl()
  expect(loaders.oneUserById).toBeDefined()
})

test('should create dataloader with runtime opts', () => {
  const loaders = dlRuntimeOpts({ db })
  expect(loaders.oneUserById).toBeDefined()
})

test('should create call dataloader', async () => {
  const spy = jest.spyOn(query, 'whereIn')
  const loaders = dl()
  const result = await loaders.oneUserById.load(1)
  expect(result).toEqual({ id: 1 })
  expect(spy).toHaveBeenCalledWith('id', [1])
  spy.mockClear()
})

test('should create call dataloader with runtime opts', async () => {
  const spy = jest.spyOn(query, 'whereIn')
  const loaders = dlRuntimeOpts({ db })
  const result = await loaders.oneUserById.load(1)
  expect(result).toEqual({ id: 1 })
  expect(spy).toHaveBeenCalledWith('id', [1])
  spy.mockClear()
})
