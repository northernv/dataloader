const DataLoader = require('dataloader')
const { join, normalize } = require('path')
const { readdirSync } = require('fs')

// Reads all the files in a directory except the `index.js`
function indexExports (dir) {
  return readdirSync(dir)
    .filter(f => f.endsWith('.js') && f !== 'index.js')
    .map(m => require(normalize(join(dir, m))))
}

// Filter out just the model files that have a `loaders()`
function getLoaderFunctions (models) {
  return models.reduce((memo, model) => {
    if (!model.default) return memo
    if (!model.default.loaders) return memo
    memo.push(model.default.loaders)
    return memo
  }, [])
}

// Find all model files
function getModels (dir) {
  return indexExports(normalize(join(process.cwd(), dir)))
}

// Custom fn to merge and throw if any keys collide
function merge (source = {}, addon = {}) {
  const keys = Object.keys(addon)
  return keys.reduce((memo, key) => {
    if (memo[key] !== undefined) throw new Error(`${key} already exists`)
    memo[key] = addon[key]
    return memo
  }, source)
}

module.exports = function loaders (modelsDir = '', options = {}) {
  const models = getModels(modelsDir)
  const loaderFunctions = getLoaderFunctions(models)

  return function init (runtimeOpts = {}) {
    // Go fetch all the loaders and store in an object
    return loaderFunctions.reduce((memo, fn) => {
      const loaders = fn(DataLoader, { ...options, ...runtimeOpts })
      return merge(memo, loaders)
    }, {})
  }
}
