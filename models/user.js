class User {
  static loaders (DataLoader, { db }) {
    const oneUserById = new DataLoader(keys => {
      return db('property')
        .whereIn('id', keys)
        .then(rows => keys.map(id => rows.find(x => x.id === id)))
    })
    return { oneUserById }
  }
}

exports.default = User
